# -*- coding: utf8 -*-
# Python Pseudo IRC Server
# (c) snowkat 2013

import re
import sys
import time
import datetime
import ConfigParser

from twisted.internet import protocol, reactor
from twisted.protocols import basic
starttime = time.time()

config = ConfigParser.RawConfigParser()
config.read('config.ini')

class user(str):
    
    def __init__(self, s):
        (self.nick, self.user, self.host) = re.split('[!@]', s)
        self = self.nick

class pseudoServer(basic.LineReceiver):
    
    def __init__(self):
        self.starttime = time.time() 
        self.modules = {}
        self.tempvar = {}
        self.init = 0
        self.userList = {}
        self.accessList = {}
        self.servers = []
        
        try:
            if sys.argv[1] == "debug":
                self.debug = True
        except IndexError:
            self.debug = False
        
        self.tempvar['autoloadModules'] = config.get('me', 'modules').split()
        self.tempvar['log channel'] = config.get('channels', 'logchan')
        self.tempvar['server name'] = config.get('me', 'name')
        self.tempvar['server info'] = config.get('me', 'info')
        self.tempvar['prefix'] = config.get('clients', 'prefix')
                
    def formatLog(self, message, level):
        timestamp = '[' + datetime.datetime.now().strftime("%a %d %H:%M:%S") + ']'
        return "%s %s" % (timestamp, message)
        if level == 2:
            sys.exit()
            
    def loadModule(self, name):
        try:
            topmodule = __import__('modules.%s.main' % name)
            module = getattr(topmodule, name).main
            instance = module.PSClient(self)
            instance.createClient()
            self.modules[name] = instance
            self.sendLine("MODE %s +h %s" % (self.tempvar['log channel'], name))
            return 0
        except AttributeError:
            return 1
        except ImportError: 
            return 2
        
    def reloadModule(self, name):
        if name in self.modules:
            self.sendLine(":%s QUIT :Module reloaded" % name)
            #py_compile.compile('modules/%s.py' % name)
            module = sys.modules.pop('modules.%s.main' % name)
            topmodule = __import__('modules.%s.main' % name)
            module = getattr(topmodule, name).main
            instance = module.PSClient(self)
            instance.createClient()
            self.modules[name] = instance

    def connectionMade(self):
        self.sendLine("PASS :%s" % config.get('uplink', 'password'))
        self.sendLine("PROTOCTL NOQUIT")
        self.sendLine("SERVER %s 1 :%s" % (self.tempvar['server name'], self.tempvar['server info']))
        self.sendLine("EOS")
        print self.formatLog("Connected to uplink server (%s:%s)" % (config.get('uplink', 'hostname'), config.get('uplink', 'port')), 1)
        
    def lineReceived(self, data):   
        #print data
            
        def readLine(line):
            line = line.split()
            if self.debug == True:
                print "[DEBUG] %s" % (data)      
                #print "[DEBUG] %s" % (line) 
            
            if "ERROR" == line[0]:
                if "No matching link configuration" in data:
                    print self.formatLog("The uplink server couldn't find a matching link block.", 2)
                if "Missing password" in data:
                    print self.formatLog("Could not send password. (Missing password)", 2)
                if "Authentication failed" in data:
                    print self.formatLog("The uplink's password doesn't match", 2)
                if ":Server" == line[1] and line[2] == self.tempvar['server name']:
                    print self.formatLog("The uplink server thinks that we are still connected.", 2)
            if "GLOBOPS" == line[1]:
                if "SQUIT" == line[3] and line[4] == self.tempvar['server name']:
                    print self.formatLog("SQUIT received by: %s %s" % (line[6], line[7]), 1)
                    uptime = time.time() - self.starttime
                    s = int(uptime)
                    timestamp = ", ".join("%d %%s"%n%t+"s"[:bool(n-1)]for t,n in zip('year  day  hour  minute  second '.split(),(s/31556926,s/86400%365,s/3600%24,s/60%60,s%60))if n>=1)
                    print self.formatLog("We have been connected to the uplink for: %s" % (timestamp), 1)

            if "PING" == line[0]:
                self.sendLine("PONG :%s" % (line[1]))
                
            if "SMO" == line[1]:
                temp = []
                print self.formatLog("Trying to auto load modules...", 1)                
                for module in self.tempvar['autoloadModules']:
                    if self.loadModule(module) == 0:
                        print self.formatLog("[%s] Succesfully loaded." % (module), 1)
                        temp.append(module)
                    if self.loadModule(module) == 1:
                        print self.formatLog("[%s] Not a valid PPIS module" % (module), 1)
                    if self.loadModule(module) == 2:
                        print self.formatLog("[%s] Module not found." % (module), 1)
                self.tempvar['autoloadModules'] = temp
                        
                print self.formatLog("Auto loaded modules: %s" % (", ".join(self.tempvar['autoloadModules'])), 1)
                print self.formatLog("Initialized and ready for use.", 1)
                
            if "SERVER" == line[0]:
                self.servers.append(line[1])
                self.tempvar['uplink server'] = line[1]
                self.tempvar['uplink numeric'] = line[2]
                self.tempvar['uplink info'] = " ".join(line[4:])
                
            if "SERVER" == line[1]:
                self.servers.append(line[2])
                
            if "NETINFO" == line[0]:
                self.tempvar['network name'] = line[8][1:]
                self.init = 1           
                
            if "MODULE" == line[1]:
                print self.formatLog("\"MODULE\" requested by %s" % (line[0][1:]), 1)
                self.sendLine("NOTICE %s :Loaded modules: %s" % (line[0][1:], " ".join(self.tempvar['autoloadModules'])))
                
            if "NICK" == line[0]:
                username = line[1]
                hostmask = "%s!%s@%s" % (line[1], line[4], line[5])
                self.userList[username.lower()] = hostmask
                if self.init == 1:
                    for module in self.modules.values():
                        function = module.getMethod('NICK')
                        function(data)
                        
            if "MODE" == line[1]:
                if not "#" in line[2]:
                    user = line[0][1:]
                    modes = line[3][1:]
                    if "o" in modes:
                        self.accessList[user] = "oper"
                    if not "o" in modes:
                        try:
                            del self.accessList[user]
                        except KeyError:
                            pass
                else:
                    if self.init == 1:
                        for module in self.modules.values():
                            function = module.getMethod('MODE')
                            function(data)                
                        
            if "NICK" == line[1]:
                oldnick = line[0][1:]
                newnick = line[2]
                self.userList[newnick.lower()] = self.userList[oldnick.lower()]
                del self.userList[oldnick.lower()]
                
            if "QUIT" == line[1] and self.init == 1:    
                for module in self.modules.values():
                    function = module.getMethod('QUIT')
                    function(data)
                    
            if "JOIN" == line[1]:
                for module in self.modules.values():
                    function = module.getMethod('JOIN')
                    function(data)    
                    
            if "PART" == line[1]:
                for module in self.modules.values():
                    function = module.getMethod('PART')
                    function(data)
                                     
            if "KICK" == line[1]:
                for module in self.modules.values():
                    function = module.getMethod('KICK')
                    function(data)
                                                       
            if "PRIVMSG" == line[1]:
                for module in self.modules.values():
                    function = module.getMethod('PRIVMSG')
                    function(data)
            
        lines = data.splitlines()
        for line in lines:
            readLine(line)
            
    def checkPermission(self, user):
        try:
            self.accessList[user]
            return "oper"
        except KeyError:
            return "user"
            
    def selfdestruct(self, user):
        self.sendLine("squit %s :%s executed selfdestruction." % (self.tempvar['server name'], user))
        print self.formatLog("Self destruction called by: %s" % (user), 1)
            
class factory(protocol.ClientFactory):
    protocol = pseudoServer

    def clientConnectionLost(self, connector, errno):
        pseudoServer().formatLog("Connetion to uplink lost.", 1)

    def clientConnectionFailed(self, connector, reason):
        pseudoServer().formatLog("Connection error: %s" % (reason), 1)

if __name__ == '__main__':
    f = factory()
    reactor.connectTCP(config.get('uplink', 'hostname'), config.getint('uplink', 'port'), f)
    reactor.run()