# -*- coding: utf8 -*-
# Module for PPIS.
# (c) snowkat 2013

import re
from modules import parser
from twisted.protocols import basic

class PSClient(basic.LineReceiver):

    def __init__(self, server):
        self.server = server
        self.parser = parser
        self.sendLine = server.sendLine
        self.hooks = {'PRIVMSG': self.handlePrivmsg,
                      'NOTICE': self.handleNotice,
                      'NICK': self.handleClientConnect, 
                      'QUIT': self.handleClientDisconnect,
                      'MODE': self.handleMode,
                      'JOIN': self.handleJoin,
                      'PART': self.handlePart,
                      'KICK': self.handleKick}
        self.module = {'author': 'snowkat', 'version': '1.1'}
        self.client = {'nickname': 'PseudoServ',
                       'ident': 'services', 
                       'vhost': 'services.bot',
                       'real name': 'Pseudo Services bot'}
        self.modes = ['no lag', 'ulined', 'auto op']
        self.clients = []
    

    def createClient(self):
        self.sendLine("NICK %s 4 0 %s %s %s 0 :%s" % (self.client['nickname'], self.client['ident'], self.client['vhost'], self.server.tempvar['server name'], self.client['real name']))
        self.sendLine(":%s JOIN %s" % (self.client['nickname'], self.server.tempvar['log channel']))
        self.server.userList[self.client['nickname'].lower()] = "%s!%s%s" % (self.client['nickname'], self.client['ident'], self.client['vhost'])
        for mode in self.modes:
            if mode == 'auto op':
                self.sendLine("MODE %s +o %s" % (self.server.tempvar['log channel'], self.client['nickname']))
            if mode == 'no lag':
                self.sendLine("SVSNOLAG + %s" % self.client['nickname'])
            setmodes = []
            if mode == 'oper':
                setmodes.append("o")
            if mode == 'ulined':
                setmodes.append("S")
            if mode == 'cloaked':
                setmodes.append("x")
            if mode == 'ssl':
                setmodes.append("z")
            self.sendLine(":%s mode %s +%s" % (self.client['nickname'], self.client['nickname'], setmodes)) 
        
    def getMethod(self, line):
        def doNothing(self):
            pass
        return self.hooks.get(line)
    
    def privmsg(self, target, message):
        self.sendLine(":%s PRIVMSG %s :%s" % (self.client['nickname'], target, message))
        
    def notice(self, target, message):
        self.sendLine(":%s NOTICE %s :%s" % (self.client['nickname'], target, message))
        
    def handlePrivmsg(self, data):
        msg = parser.IrcMsg.parse(data.strip())
        status = self.server.checkPermission(msg.prefix)
        if msg.args[0] != self.client['nickname']:
            message = " ".join(msg.args[1:])[1:].lower().split()
            if msg.args[1].startswith(self.server.tempvar['prefix']):
                if message[0] == "ping":
                    self.privmsg(msg.args[0], "Pong")
                    
                if message[0] == "tempdata" and status == "oper":
                    if status == "oper":
                        self.privmsg(msg.args[0], self.server.tempvar)
                    else:
                        self.privmsg(msg.args[0], "Premission denied for %s with %s" % (msg.prefix, message[0]))
    
                if message[0] == "reload":
                    if status == "oper":
                        try:
                            module = message[1]
                        except IndexError:
                            self.privmsg(msg.args[0], "Prefix: !reload <module name>")
                        else:
                            self.privmsg(msg.args[0], "Trying to reload module: %s " % module)
                            try:
                                self.server.reloadModule(module)
                            except NameError, err:
                                self.privmsg(msg.args[0], "Error: %s" % err)
                            except ImportError:
                                self.privmsg(msg.args[0], "Error: Failed to load modules: %s" % module)
                            except IndexError:
                                self.privmsg(msg.args[0], "Prefix: !reload <module name>")
                    else:
                        self.privmsg(msg.args[0], "Premission denied for %s with %s" % (msg.prefix, message[0]))
                       
        else:
            message = " ".join(msg.args[1:]).lower().split()
            message2 = " ".join(msg.args[1:])[1:].split()
            if message[0] == "help":
                try:
                    topic = message[1]
                except IndexError:
                    self.notice(msg.prefix, "The following commands are available:")
                    self.notice(msg.prefix, "For more info about a topic use: HELP <topic>")
                    self.notice(msg.prefix, " ")
                    self.notice(msg.prefix, "CLIENT          Manage pseudo clients.")
                    self.notice(msg.prefix, "SERVER          Manage pseudo servers.")
                    self.notice(msg.prefix, "MODULE          Manage the modules.")
                    
                else:
                    if topic == "client":
                        try:
                            subtopic = message[2]
                        except IndexError:
                            self.notice(msg.prefix, "Syntax: CLIENT <action> <parameters>")
                            self.notice(msg.prefix, " ")
                            self.notice(msg.prefix, "Actions:")
                            self.notice(msg.prefix, "ADD, DEL, JOIN, PART")
                            self.notice(msg.prefix, " ")
                            self.notice(msg.prefix, "use \"/msg pseudoserv help client <action>\" for more information.")
                        else:
                            if subtopic == "add":
                                self.notice(msg.prefix, "Syntax: ADD <nickname> <ident> <vhost> <real name> [server]")
                                self.notice(msg.prefix, "Example: ADD LolServ LOL lol.bot Lol bot py.network.com")                        
                            if subtopic == "del":
                                self.notice(msg.prefix, "Syntax: DEL <nickname>")
                                self.notice(msg.prefix, "Example: DEL LolServ")
                            if subtopic == "join":
                                self.notice(msg.prefix, "Syntax:  JOIN <client> <channel>")
                                self.notice(msg.prefix, "Example: JOIN PieServ #pie")
                            if subtopic == "join":
                                self.notice(msg.prefix, "Syntax:  PART <client> <channel>")
                                self.notice(msg.prefix, "Example: PART PieServ #pie")
                            
                    if topic == "server":
                        try:
                            subtopic = message[2]
                        except IndexError:
                            self.notice(msg.prefix, "Syntax: SERVER <action> <parameters>")
                            self.notice(msg.prefix, " ")
                            self.notice(msg.prefix, "Actions:")
                            self.notice(msg.prefix, "ADD, DEL")
                            self.notice(msg.prefix, " ")
                            self.notice(msg.prefix, "use \"/msg pseudoserv help server <action>\" for more information.")
                        else:
                            if subtopic == "add":
                                self.notice(msg.prefix, "Syntax: ADD <server name> <numeric> <description>")
                                self.notice(msg.prefix, "Example: ADD py.network.com 24 Python pseudo server.")                        
                            if subtopic == "del":
                                self.notice(msg.prefix, "Syntax:  DEL <server name>")
                                self.notice(msg.prefix, "Example: DEL py.network.com")
                                
                    if topic == "module":
                        try:
                            subtopic = message[2]
                        except IndexError:
                            self.notice(msg.prefix, "Syntax: MODULE <action> <module name>")
                            self.notice(msg.prefix, " ")
                            self.notice(msg.prefix, "Actions:")
                            self.notice(msg.prefix, "LOAD, RELOAD, UNLOAD")
                            self.notice(msg.prefix, " ")
                            self.notice(msg.prefix, "use \"/msg pseudoserv help module <action>\" for more information.")
                        else:
                            if subtopic == "load":
                                self.notice(msg.prefix, "Syntax: LOAD <module name>")
                                self.notice(msg.prefix, "Example: LOAD <module name>")                        
                            if subtopic == "reload":
                                self.notice(msg.prefix, "Syntax: RELOAD <module name>")
                                self.notice(msg.prefix, "Example: RELOAD <module name>")
                                
            if message[0] == "module":
                try:
                    sub = message[1]
                except IndexError:
                    self.notice(msg.prefix, "Syntax: MODULE option parameters")
                    self.notice(msg.prefix, "/msg PseudoServ HELP MODULE for more information.")
                else:
                    if sub == "load":
                        #try:
                        message[2]
                        if message[2] in self.server.modules:
                            self.notice(msg.prefix, "The module you try to load is already loaded.")
                        else:
                            self.notice(msg.prefix, self.server.loadModule(message[2]))
                        #except:
                        #    self.notice(msg.prefix, "Insufficient parameters for MODULE LOAD.")
                        #    self.notice(msg.prefix, "Syntax: MODULE LOAD <module name>")                            
                                                                
            if message[0] == "client":
                try:
                    sub = message[1]
                except IndexError:
                    self.notice(msg.prefix, "Syntax: CLIENT option parameters")
                    self.notice(msg.prefix, "/msg PseudoServ HELP CLIENT for more information.")
                else:
                    if sub == "add":
                        try: 
                            message[4]
                            if message[2] != " " and message[4] != " ":
                                pass
                            if message[2].lower() not in self.clients and message[2].lower() not in self.server.userList:
                                self.sendLine("NICK %s 4 0 %s %s %s 0 :%s" % (message2[2], message2[3], message2[4], self.server.tempvar['server name'], " ".join(message2[5:])))
                                self.privmsg(self.server.tempvar['log channel'], "CLIENT: %s!%s@%s [%s] created by: %s" % (message2[2], message2[3], message2[4], " ".join(message2[5:]), msg.prefix)) 
                                self.clients.append(message[2].lower())
                            else:
                                self.notice(msg.prefix, "Nickname is already in use.")
                        except: 
                            self.notice(msg.prefix, "Insufficient parameters for CLIENT ADD.")
                            self.notice(msg.prefix, "Syntax: CLIENT ADD <client name> <ident> <vhost> <real name> [server]")
                            
                    if sub == "del":
                        try: 
                            message[3]
                            if message[2].lower() not in self.clients:
                                self.notice(msg.prefix, "Client does not exist.")
                            else:
                                self.sendLine(":%s kill %s :%s" % (message[2], message[2], message[3]))
                                self.privmsg(self.server.tempvar['log channel'], "CLIENT: %s destroyed by: %s" % (message[2], msg.prefix))
                                self.clients.remove(message[2].lower())
                        except: 
                            self.notice(msg.prefix, "Insufficient parameters for CLIENT DELL.")
                            self.notice(msg.prefix, "Syntax: CLIENT DEL <client name> <reason>")
                            
                    if sub == "join":
                        try: 
                            message[3]
                            if "#" in message[3]:
                                if message[2].lower() not in self.clients:
                                    self.notice(msg.prefix, "Client does not exist.")
                                else:
                                    self.sendLine(":%s JOIN %s" % (message[2], message[3]))
                                    self.privmsg(self.server.tempvar['log channel'], "CLIENT %s joined %s (%s)" % (message[2], message[3], msg.prefix))
                            else: 
                                self.notice(msg.prefix, "Channel name should include a # character")
                        except IndexError: 
                            self.notice(msg.prefix, "Insufficient parameters for CLIENT JOIN.")
                            self.notice(msg.prefix, "Syntax: CLIENT JOIN <client name> <channel>")
                            
                    if sub == "part":
                        try: 
                            message[3]
                            if "#" in message[3]:
                                if message[2].lower() not in self.clients:
                                    self.notice(msg.prefix, "Client does not exist.")
                                else:
                                    self.sendLine(":%s PART %s" % (message[2], message[3]))
                                    self.privmsg(self.server.tempvar['log channel'], "CLIENT %s left %s (%s)" % (message[2], message[3], msg.prefix))
                            else: 
                                self.notice(msg.prefix, "Channel name should include a # character")
                        except IndexError: 
                            self.notice(msg.prefix, "Insufficient parameters for CLIENT PART.")
                            self.notice(msg.prefix, "Syntax: CLIENT PART <client name> <channel>")                        
                                           
            if message[0] == "server":
                try:
                    sub = message[1]
                except IndexError:
                    self.notice(msg.prefix, "Syntax: SERVER option parameters")
                    self.notice(msg.prefix, "/msg PseudoServ HELP SERVER for more information.")
                else:
                    if sub == "add":
                        try:
                            message[4]
                            if message[2] not in self.server.servers:
                                if message[3].isdigit() == True:
                                    if message[2] == self.server.tempvar['server name']:
                                        self.notice(msg.prefix, "Nice try :)")
                                    else:
                                        pattern = "/^[a-zA-Z0-9]+$/"
                                        com_pattern = re.compile(pattern)
                                        if com_pattern.match(message[2]):
                                            self.sendLine("SERVER %s %s :%s" % (message[2], message[3], " ".join(message[4:])))
                                            self.privmsg(self.server.tempvar['log channel'], "Introducing %s (Added by: %s)" % (message[2], msg.prefix))
                                            self.server.servers.append(message[2])
                                        else:
                                            self.notice(msg.prefix, "Not a valid server name.")
                                else:
                                    self.notice(msg.prefix, "Error: the numeric may only certain numbers...")
                            else:
                                self.notice(msg.prefix, "server %s already exists." % message[2])
                        except IndexError:
                            self.notice(msg.prefix, "Insufficient parameters for SERVER ADD.")
                            self.notice(msg.prefix, "Syntax: SERVER ADD <server name> <numeric> <description>")
                            
                    if sub == "del":
                        try:
                            message[2]
                            if message[2] == self.server.tempvar['server name']:
                                self.notice(msg.prefix, "No. I surrender!, Please do not kill me! D:")
                            elif message[2] not in self.server.servers:
                                self.notice(msg.prefix, "Server does not exist...")

                            else:
                                self.sendLine("SQUIT %s :Removed by %s" % (message[2], msg.prefix))
                                self.privmsg(self.server.tempvar['log channel'], "server %s deleted by %s" % (message[2], msg.prefix))
                                self.server.servers.remove(message[2])         
                        except:
                            self.notice(msg.prefix, "Insufficient parameters for SERVER DEL.")
                            self.notice(msg.prefix, "Syntax: SERVER DEL <server name>")           
                                
    def handleMode(self, data): 
        pass
    
    def handleKick(self, data):
        pass
    
    def handleJoin(self, data):
        pass
    
    def handlePart(self, data):
        pass
                         
    def handleNotice(self, data): 
        pass
        
    def handleClientConnect(self, data):
        pass
    
    def handleClientDisconnect(self, data):
        pass
    