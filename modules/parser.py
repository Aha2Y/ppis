#IrcMsg class written by Dunj3/Kingdread.

class IrcMsg(object):
    def __init__(self, prefix, command, args):
        if any(" " in arg for arg in args[:-1]):
            raise ValueError("Only the last element may contain spaces")
        self.prefix = prefix
        self.command = command
        self.args = args
    def __str__(self):
        res = []
        if self.prefix:
            res.append(":%s" % self.prefix)
            res.append(self.command)
        for arg in self.args:
            if " " in arg:
                res.append(":%s" % arg)
                continue
            res.append(arg)
        return " ".join(res)
    def __repr__(self):
        return "IrcMsg(%r, %r, %r)" % (self.prefix, self.command, self.args)
 
    @classmethod
    def parse(cls, string):
        if string.startswith(":"):
            prefix, string = string.split(" ", 1)
            prefix = prefix[1:]
        else:
            prefix = None
        _ = string.split(" :", 1)
        args = _[0].split(" ")
        try:
            args.append(_[1])
        except IndexError:
            pass
        return cls(prefix, args[0], args[1:])